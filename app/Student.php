<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //Table name
	protected $table = 'students';
	
	//Primary key
	public $primaryKey = 'st_id';
	
	//Time stamp
	public $timestamps = TRUE;
}
