<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Info_student extends Model
{
    //Table name
	protected $table = 'info_students';
	
	//Primary key
	public $primaryKey = 'is_id';
	
	//Time stamp
	public $timestamps = TRUE;
}
