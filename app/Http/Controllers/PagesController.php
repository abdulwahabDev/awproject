<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){
		$title = 'Welcome to myProject';
		return view('pages.index',compact('title'));
	}
	
	
	public function student_login(){
		$title = 'SignIn for Students';
		return view('pages.student_login')->with('title',$title);
	}
	
	public function student_signup(){
		$title = 'Signup for Students';
		
		$data = array(
			'title' => 'Signup for Students',
			'city' => 'City',
			'countries' => ['pakistan','india','china','bangladesh'],
			'cities' => ['karachi','lahore','islamabad','peshawar']
		);
		return view('pages.student_signup')->with($data);
	}
}
