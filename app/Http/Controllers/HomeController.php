<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Info_student;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$id = Info_student::where('is_st_id', 1);
		
		$user = DB::table('info_students')->select('is_st_id')->where('is_st_id', '=', Auth::id())->get();
		
		//print_r($user[0]);
		
		if(empty($user[0])){
			
			echo "<br>";
			echo "testtest";
			echo "<br>";
			return redirect('student-set-language');
		}
		
		return view('home')->with('user_info',$user);
    }
	
}
