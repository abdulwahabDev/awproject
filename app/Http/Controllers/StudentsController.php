<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Student;
use App\Info_student;
use App\Student_course;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //	
    }
	
	public function set_language(){
		
		$data = array(
			'languages' => DB::table('languages')->get()
		);
		return view('student_language')->with($data);
	}
	
	public function language_submit(Request $request){
		echo '';
	}
	
	public function set_profile(){
		
		$data = array(
			'languages' => DB::table('languages')->get(),
			'qualification' => DB::table('qualification')->get(),
			'countries' => DB::table('country')->get(),
			'cities' => DB::table('city')->get(),
			'courses' => DB::table('courses')->get()
		);
		
		return view('student_profile')->with($data);
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$data = array(
			'title' => 'Signup for Students',
			'city' => 'City',
			'countries' => ['pakistan','india','china','bangladesh'],
			'cities' => ['karachi','lahore','islamabad','peshawar']
		);
		return view('pages.student_signup')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	
	public function store(Request $request)
    {
		
		if($request->input('info')){
			
			$Info_student = new Info_student;
			
			$this->validate($request, [
				'age' => 'required',
				'phone' => 'required',
				'gender' => 'required',
				'country' => 'required',
				'city' => 'required',
				'qualification' => 'required',
				'institute' => 'required',
				'from' => 'required',
				'to' => 'required',
				'student_info' => 'required'
			]);
			$photoName = time().'.'.$request->student_pic->getClientOriginalExtension();
			
			$request->student_pic->move(public_path('images'), $photoName);
			
			$Info_student->is_pic = $photoName;
			$Info_student->is_st_id = $request->input('student_id');
			$Info_student->is_dob = $request->input('age');
			$Info_student->is_phone = $request->input('phone');
			$Info_student->is_gender = $request->input('gender');
			$Info_student->is_country = $request->input('country');
			$Info_student->is_city = $request->input('city');
			$Info_student->is_qualification = $request->input('qualification');
			$Info_student->is_institute = $request->input('institute');
			$Info_student->is_from = $request->input('from');
			$Info_student->is_to = $request->input('to');
			$Info_student->is_st_intro = $request->input('student_info');
		
			$Info_student->save();
			
		} else if($request->input('course_info')){
			
			$Student_course = new Student_course;
			
			$this->validate($request, [
				'language' => 'required',
				'course' => 'required',
				'level' => 'required'
			]);
			$Student_course->sc_st_id = $request->input('student_id');
			$Student_course->sc_course = $request->input('language');
			$Student_course->sc_language = $request->input('course');
			$Student_course->sc_level = $request->input('level');
			
			$Student_course->save();
		}
		
		//return redirect('student/create')->with('success', "You are registered successfully");
    } 
	 
    /*public function store(Request $request)
    {
        $this->validate($request, [
			'email' => 'required',
			'pwd' => 'required',
			'city' => 'required',
			'country' => 'required'
		]);
		$student = new Student;
		
		$student->st_email = $request->input('email');
		$student->st_pass = $request->input('pwd');
		$student->st_city = $request->input('city');
		$student->st_country = $request->input('country');
		$student->save();
		return redirect('student/create')->with('success', "You are registered successfully");
    }*/

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
