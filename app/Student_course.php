<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student_course extends Model
{
     //Table name
	protected $table = 'student_courses';
	
	//Primary key
	public $primaryKey = 'sc_id';
	
	//Time stamp
	public $timestamps = TRUE;
}
