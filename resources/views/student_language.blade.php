@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="container">
		<h1>Step 1:</h1>
		<hr>
			<h2>Select Language</h2>

			  {!! Form::open(['action' => 'StudentsController@language_submit', 'method'=> 'POST', 'class' => 'form-horizontal']) !!}
				<input type="hidden" name="student_id" value="{{ Auth::id() }}"> 
				<div class="row">
					<div class="col-sm-4">
					<div class="form-group">
					  <label class="control-label col-sm-6">Select Language:</label>
						<select class="form-control" name="language" >
							<option value="">-select-</option>
							@foreach($languages as $language)
								<option value="{{ $language->l_id }}">{{ $language->l_language }}</option>
							@endforeach
						</select>
					</div>
					</div>
				</div>
				<div class="row">
				<div class="form-group">        
				  <div class="col-sm-offset-2 col-sm-10">
					<input type="submit" class="btn btn-info  btn-lg" name="submit_language" value="Select" />
				  </div>
				</div>
				</div>
			  {!! Form::close() !!}
		</div>
    </div>
</div>
@endsection
