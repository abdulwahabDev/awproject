@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="container">
		<h1>Step 2:</h1>
		<hr>
			<h2>Profile</h2>
			  {!! Form::open(['action' => 'StudentsController@store', 'method'=> 'POST', 'files' => true , 'class' => 'form-horizontal']) !!}
				
				<div class="row">
				
				<div class="col-sm-4">
					<div class="form-group">
					  <label class="control-label col-sm-5" for="age">Upload picture:</label>
						{!! Form::file('student_pic', ['class'=>'form-control']) !!}	
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
					  <label class="control-label col-sm-4" for="age">DOB:</label>
						<input type="date" class="form-control" id="age" name="age">
						<input type="hidden" name="student_id" value="{{ Auth::id() }}"> 
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
					  <label class="control-label col-sm-4" for="age">Cell Phone:</label>
						<input type="text" class="form-control" id="age" name="phone" placeholder="Enter phone number" >
					</div>
				</div>
				</div>
				<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
					  <label class="control-label col-sm-4" for="age">Gender:</label>
						<select class="form-control" name="gender">
							<option value="">-select</option>
							<option value="male">Male</option>
							<option value="female">Female</option>
						</select>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
					  <label class="control-label col-sm-4" for="age">Country:</label>
						<select class="form-control" name="country" >
							<option value="">-select-</option>
							@foreach($countries as $country)
								<option value="{{ $country->country_id }}">{{ $country->co_country }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
					  <label class="control-label col-sm-4" for="age">City:</label>
						<select class="form-control" name="city" >
							<option value="">-select-</option>
							@foreach($cities as $city)
								<option value="{{ $city->city_id }}">{{ $city->ci_city }}</option>
							@endforeach
						</select>
					</div>
				</div>
				</div>
				<br>
				
				
				<div class="form-group">
					<h2>Academic Record:</h2>
					<div class="row">
						<div class="col-sm-3">          
							<label class="control-label col-sm-4">Qualification:</label>
							<select class="form-control" name="qualification">
								<option value="">-select-</option>
								@foreach($qualification as $value)
									<option value="{{ $value->qu_id }}">{{ $value->qu_qualification }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-sm-3">          
							<label class="control-label col-sm-4" for="ins">Institute:</label>
							<input type="text" class="form-control" id="ins" placeholder="Enter Institute" name="institute">
						</div>
						<div class="col-sm-3">          
							<label class="control-label col-sm-4" for="pwd">From:</label>
							<select class="form-control" name="from">
								<option value="">-select-</option>
								@for ($i = 2018; $i >= 1970; $i--)
									<option value="{{ $i }}">{{ $i }}</option>
								@endfor
							</select>
						</div>
						<div class="col-sm-3">          
							<label class="control-label col-sm-4" for="pwd">To:</label>
							<select class="form-control" name="to">
								<option value="">-select-</option>
								<option value="progress">In Progress</option>
								@for ($i = 2018; $i >= 1970; $i--)
									<option value="{{ $i }}">{{ $i }}</option>
								@endfor
							</select>
						</div>
					</div>
				</div>
				<br>
				<br>
				<div class="form-group">
					<div class="row">
					<h2>Student Short Introduction:</h2>
					<div class="form-group">
						<textarea class="form-control"  name="student_info" rows="10" cols="155" ></textarea>
					</div>
					</div>
				</div>

				<div class="form-group">        
				  <div class="col-sm-offset-2 col-sm-10">
					<input type="submit" class="btn btn-info  btn-lg" name="info" />
				  </div>
				</div>
			  {!! Form::close() !!}
			<br>
			<br>
			<hr>
			<br>
			<br>
			
			<h2>Language & Course</h2>
			<br>
			<br>
			  {!! Form::open(['action' => 'StudentsController@store', 'method'=> 'POST', 'class' => 'form-horizontal']) !!}
				<input type="hidden" name="student_id" value="{{ Auth::id() }}"> 
				<div class="row">
					<div class="col-sm-4">
					<div class="form-group">
					  <label class="control-label col-sm-6">Select Language:</label>
						<select class="form-control" name="language" >
							<option value="">-select-</option>
							@foreach($languages as $language)
								<option value="{{ $language->l_id }}">{{ $language->l_language }}</option>
							@endforeach
						</select>
					</div>
					</div>

					<div class="col-sm-4">
					<div class="form-group">
					  <label class="control-label col-sm-7">Course you want to learn:</label>
						<select class="form-control" name="course" >
							<option value="">-select-</option>
							@foreach($courses as $course)
								<option value="{{ $course->cr_id }}">{{ $course->cr_course }}</option>
							@endforeach
						</select>
					</div>
					</div>
					
					<div class="col-sm-4">
					<div class="form-group">
					  <label class="control-label col-sm-6">Course level:</label> 
						<select class="form-control" name="level" >
							<option value="">-select-</option>
								<option value="beginner">Beginner</option>
								<option value="intermediate">Intermediate</option>
								<option value="advance">Advance</option>
						</select>
					</div>
					</div>
					
					
				<div class="form-group">        
				  <div class="col-sm-offset-2 col-sm-10">
					<input type="submit" class="btn btn-info  btn-lg" name="course_info" />
				  </div>
				</div>
				</div>
			  {!! Form::close() !!}
		</div>
    </div>
</div>
@endsection
