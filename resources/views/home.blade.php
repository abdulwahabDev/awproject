@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
					<a href="student-profile-setup" class="btn btn-info" role="button">Set up your profile</a>
					
					<p>
						@foreach($user_info as $value )
							{{ $value->is_st_id }}
						@endforeach
					</p>
					
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
