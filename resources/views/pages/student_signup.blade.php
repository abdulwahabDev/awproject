@extends('layouts.app')
		
@section('content')
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Online teaching</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="#">Student</a></li>
      <li><a href="#">Teacher</a></li>
      <li><a href="#">Admin</a></li>
    </ul>
	<ul class="nav navbar-nav navbar-right">
		<li><a href="student-login"><span class="glyphicon glyphicon-user"></span> Sign IN</a></li>
    </ul>
  </div>
</nav>


<div class="container">
  <h2>Student Signup Form</h2>
  {!! Form::open(['action' => 'StudentsController@store', 'method'=> 'POST', 'class' => 'form-horizontal']) !!}
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-5">
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
      </div>
    </div>
	
    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Password:</label>
      <div class="col-sm-5">          
        <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
      </div>
    </div>
	
	<div class="form-group">
      <label class="control-label col-sm-2" for="pwd">{{$city}} :</label>
      <div class="col-sm-5">
		<select class="form-control" name='city'>
			@foreach($cities as $city)
				<option value="{{$city}}" class="form-controol">{{$city}}</option>
			@endforeach
		</select>
	  </div>
    </div>
	
	<div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Country:</label>
      <div class="col-sm-5">          
        <select class="form-control" name='country'>
			@foreach($countries as $country)
				<option value="{{$country}}" class="form-controol">{{$country}}</option>
			@endforeach
		</select>
      </div>
    </div>

    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>
  {!! Form::close() !!}
  
</div>
@endsection
