<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_students', function (Blueprint $table) {
            $table->increments('is_id');
			
			$table->string('is_st_id',255);
            $table->date('is_dob');
            $table->string('is_phone',20);
            $table->string('is_gender',8);
            $table->string('is_city',20);
            $table->string('is_country',20);
			
			$table->string('is_qualification',20);
			$table->string('is_institute',20);
			$table->string('is_from',20);
			$table->string('is_to',20);
			$table->text('is_st_intro');
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_students');
    }
}
