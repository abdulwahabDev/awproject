<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeAllFieldsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('info_students', function (Blueprint $table) {
            $table->string('is_st_id')->nullable()->change();
            $table->date('is_dob')->nullable()->change();
            $table->string('is_phone')->nullable()->change();
            $table->string('is_gender')->nullable()->change();
            $table->string('is_city')->nullable()->change();
            $table->string('is_from')->nullable()->change();
            $table->string('is_to')->nullable()->change();
            $table->text('is_st_intro')->nullable()->change();
        });
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('info_students', function (Blueprint $table) {
            //
        });
    }
}
