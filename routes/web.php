<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/student-signup', 'PagesController@student_signup');
Route::get('/student-login', 'PagesController@student_login');
Route::get('/student-profile-setup', 'StudentsController@set_profile');
Route::get('/student-set-language', 'StudentsController@set_language');
Route::get('/language-selected', 'StudentsController@language_submit');
Route::resource('student', 'StudentsController');

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
